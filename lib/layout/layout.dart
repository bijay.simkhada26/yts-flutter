import 'package:flutter/material.dart';
import 'package:yts_flutter/screens/pages/home.dart';
import 'package:yts_flutter/screens/pages/trending.dart';
import 'package:yts_flutter/screens/widgets/bottomNavigation.dart';
import 'package:yts_flutter/screens/widgets/topNav.dart';
import 'package:yts_flutter/utils/theme.dart';

class AppLayout extends StatefulWidget {
  const AppLayout({Key? key}) : super(key: key);

  @override
  State<AppLayout> createState() => _AppLayoutState();
}

class _AppLayoutState extends State<AppLayout> {

  final List<Widget> _pages = [
    HomeScreen(),
    TrendingScreen(),
  ];

  int _selecteIndex = 0;

  void _changeSelectedIndex(index){
    setState(() {
      _selecteIndex=index;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: TopNav(),
      ),
      body: _pages[_selecteIndex],
      bottomNavigationBar: BottomNav(_selecteIndex, _changeSelectedIndex),
    );
  }
}
