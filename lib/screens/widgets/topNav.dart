import 'package:flutter/material.dart';
import 'package:yts_flutter/utils/theme.dart';

class TopNav extends StatelessWidget {
  const TopNav({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.black87,
      title: Text('YTS', style: kPrimaryHeading1,),
      centerTitle: true,
      leading: Container(
        margin: const EdgeInsets.only(left: 15),
        child: Image.asset('assets/images/logo2.png'),
      ),
    );
  }
}
