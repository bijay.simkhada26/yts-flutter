import 'package:flutter/material.dart';
import 'package:yts_flutter/utils/theme.dart';

class BottomNav extends StatelessWidget {

  final int _selectedIndex;
  final Function _changeIndex;

  const BottomNav( this._selectedIndex, this._changeIndex, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context){
    return BottomNavigationBar(
        elevation: 3,
        showUnselectedLabels: false,
        showSelectedLabels: true,
        backgroundColor: Colors.black87,
        unselectedItemColor: Colors.grey,
        selectedItemColor: kPrimaryColor,
        currentIndex: _selectedIndex,
        iconSize: 30,
        onTap: (i){
          _changeIndex(i);
        },
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.home_outlined), label: 'Home'),
          BottomNavigationBarItem(icon: Icon(Icons.trending_up_outlined), label: 'Trending'),
        ]);
  }

}
