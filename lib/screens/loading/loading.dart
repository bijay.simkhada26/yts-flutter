import 'package:flutter/material.dart';
import 'package:yts_flutter/layout/layout.dart';
import 'package:yts_flutter/screens/pages/home.dart';
import 'package:yts_flutter/utils/theme.dart';

class LoadingScreen extends StatefulWidget {
  const LoadingScreen({Key? key}) : super(key: key);

  @override
  State<LoadingScreen> createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {

  @override
  void initState(){
    super.initState();
    _navigateToHome();
  }

  _navigateToHome() async{
    await Future.delayed(const Duration(milliseconds: 1800), (){});
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> AppLayout()));
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(body: SafeArea(

      child: Container(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        color: Colors.black87,
        child:  Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/images/logo.png', height: 200,),
            Text('YTS', style: kPrimaryHeading1,)
          ],
        )),
      )
    );
  }
}
