import 'package:flutter/material.dart';
import 'package:yts_flutter/screens/widgets/topNav.dart';
import 'package:yts_flutter/utils/theme.dart';

class TrendingScreen extends StatefulWidget {
  const TrendingScreen({Key? key}) : super(key: key);

  @override
  _TrendingScreenState createState() => _TrendingScreenState();
}

class _TrendingScreenState extends State<TrendingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Text('Trending Screen', style: kPrimaryHeading2,),
    );
  }
}
