import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:yts_flutter/screens/widgets/topNav.dart';
import 'package:yts_flutter/utils/theme.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          color: kSecondaryColor,
          padding: const EdgeInsets.all(25),
          width: double.infinity,
          height: 400,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Center(
                  child: Text(
                'Popular Downloads',
                style: kPrimaryHeading3,
              )),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(children: [
                  Container(
                    height: 200,
                    width: 150,
                    margin: const EdgeInsets.only(right: 20),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        image: const DecorationImage(
                          image: NetworkImage(
                              'https://wallpaperaccess.com/full/21788.png'),
                          fit: BoxFit.cover,
                        )),
                    child: Stack(
                      children: [
                        Positioned(
                            bottom: 0,
                            child: Container(
                              height: 75,
                              width: 150,
                              color: kPrimaryColor,
                              padding: const EdgeInsets.all(10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    'Deadpool',
                                    style: kWhiteText,
                                  ),
                                  Text(
                                    '2022',
                                    style: kWhiteText,
                                  ),
                                ],
                              ),
                            ))
                      ],
                    ),
                  ),
                ]),
              )
            ],
          )),
    );
  }
}
