import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

var kPrimaryColor = Colors.green;

var kSecondaryColor = Colors.black87;

var kPrimaryHeading1 = GoogleFonts.poppins(
  textStyle: const TextStyle(
    fontSize: 22,
    fontWeight: FontWeight.bold,
    color: Colors.green,
  )
);

var kPrimaryHeading2 = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.bold,
      color: Colors.green,
    )
);

var kPrimaryHeading3 = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.bold,
      color: Colors.green,
    )
);

var kSecondaryHeading1 = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 22,
      fontWeight: FontWeight.bold,
      color: Colors.grey,
    )
);

var kSecondaryHeading2 = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.bold,
      color: Colors.grey,
    )
);

var kSecondaryHeading3 = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.bold,
      color: Colors.grey,
    )
);

var kWhiteText = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.bold,
      color: Colors.white,
    )
);

var kCardDecoration = BoxDecoration(
    borderRadius: BorderRadius.circular(10),
    color: Colors.white,
    boxShadow: const [
      BoxShadow(
          color: Colors.black12,
          offset: Offset(0, 2.0),
          blurRadius: 10,
          spreadRadius: 2)
    ]
);
