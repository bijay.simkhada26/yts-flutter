import 'package:flutter/material.dart';
import 'package:yts_flutter/screens/loading/loading.dart';

void main(){
  runApp(const YtsApp());
}

class YtsApp extends StatelessWidget{
  const YtsApp({Key? key}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'YTS',
      home: LoadingScreen(),
    );
  }
}